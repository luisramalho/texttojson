package com.luisramalho.texttojson.espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.luisramalho.texttojson.MainActivity;
import com.luisramalho.texttojson.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Luís Ramalho on 22/01/17.
 * <info@luisramalho.com>
 */

@RunWith(AndroidJUnit4.class)
public class ConvertMessageTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void convertEmpty() {
        String actual = "";
        onView(withId(R.id.messageEditText))
                .perform(clearText(), typeText(actual), closeSoftKeyboard());
        onView(withId(R.id.convertButton)).perform(click());
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(getResourceString(R.string.nothing_to_parse))))
                .check(matches(isDisplayed()));
    }

    @Test
    public void convertJustText() {
        String expected = "{}";
        String actual = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithMention() {
        String expected = "{\"mentions\":[\"chris\"]}";
        String actual = "@chris you around?";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithTooLongMention50Chars() {
        String expected = "{}";
        String actual = "@chrisTooLongUsernameHhHboPGor8zLUXWpKx4B1PRT4lh3gv";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithLongestMentionPossible49Chars() {
        String expected = "{\"mentions\":[\"chrislJ8V4e04Zf2jCCjKe3j1Js00IyqTgDs4rjhVjcJqNCvR\"]}";
        String actual = "@chrislJ8V4e04Zf2jCCjKe3j1Js00IyqTgDs4rjhVjcJqNCvR";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithIDuplicatedMentions() {
        String expected = "{\"mentions\":[\"chris\"]}";
        String actual = "@chris @chris you around?";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTestInvalidMention() {
        String expected = "{}";
        // Empty
        String actual = "@";
        performConvertAction(expected, actual);
        // Random not word characters
        actual = "@-chris";
        performConvertAction(expected, actual);
        actual = "@%chris";
        performConvertAction(expected, actual);
        actual = "@&chris";
        performConvertAction(expected, actual);
        actual = "@=chris";
        performConvertAction(expected, actual);
    }

    @Test public void convertTestValidMention() {
        String expected = "{\"mentions\":[\"chris\"]}";
        // Ll (Letter, Lowercase)
        String actual = "@chris";
        performConvertAction(expected, actual);
        // Lu (Letter, Uppercase)
        expected = "{\"mentions\":[\"CHRIS\"]}";
        actual = "@CHRIS";
        performConvertAction(expected, actual);
        // Lt (Letter, Titlecase)
        expected = "{\"mentions\":[\"Chris\"]}";
        actual = "@Chris";
        performConvertAction(expected, actual);
        // Nd (Number, Decimal Digit)
        expected = "{\"mentions\":[\"1chris\"]}";
        actual = "@1chris";
        performConvertAction(expected, actual);
        // Low line _
        expected = "{\"mentions\":[\"_chris\"]}";
        actual = "@_chris";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithLink() {
        String expected = "{\"links\":[{\"title\":\"2018 PyeongChang Olympic Games | NBC Olympics\",\"url\":\"http://www.nbcolympics.com\"}]}";
        String actual = "Olympics are starting soon; http://www.nbcolympics.com";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithDuplicatedLinks() {
        String expected = "{\"links\":[{\"title\":\"2018 PyeongChang Olympic Games | NBC Olympics\",\"url\":\"http://www.nbcolympics.com\"}]}";
        String actual = "Olympics are starting soon; http://www.nbcolympics" +
                ".com http://www.nbcolympics.com";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithValidEmoticons() {
        String expected = "{\"emoticons\":[\"coffee\",\"strictly15chars\"]}";
        String actual = "Good morning! (Strictly15Chars) (coffee)";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithDuplicatedEmoticons() {
        String expected = "{\"emoticons\":[\"coffee\",\"strictly15chars\"]}";
        String actual = "Good morning! (Strictly15Chars) (coffee) (coffee)";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithInvalidEmoticon_longerThan15Characters() {
        String expected = "{\"emoticons\":[\"coffee\"]}";
        String actual = "Good morning! (longerThan15Chars) (coffee)";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithInvalidEmoticon_withSpace() {
        String expected = "{\"emoticons\":[\"coffee\"]}";
        String actual = "Good morning! (with space) (coffee)";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithInvalidEmoticon_notAlphanumeric() {
        String expected = "{\"emoticons\":[\"coffee\"]}";
        String actual = "Good morning! (&) (coffee)";
        performConvertAction(expected, actual);
    }

    @Test
    public void convertTextWithInvalidEmoticon_empty() {
        String expected = "{\"emoticons\":[\"coffee\"]}";
        String actual = "Good morning! () (coffee)";
        performConvertAction(expected, actual);
    }

    /**
     * Tests the typing and clicking of button in UI.
     *
     * @param expected the expected string;
     * @param actual the actual string.
     */
    private void performConvertAction(String expected, String actual) {
        onView(withId(R.id.messageEditText))
                .perform(clearText(), typeText(actual), closeSoftKeyboard());
        onView(withId(R.id.convertButton)).perform(click());
        onView(withId(R.id.jsonTextView)).check(matches(withText(expected)));
    }

    /**
     * Gets the string resource.
     *
     * @param id the id of the string.
     * @return the string resource.
     */
    private String getResourceString(int id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        return targetContext.getResources().getString(id);
    }
}
