package com.luisramalho.texttojson.models;

import java.io.Serializable;

/**
 * Created by Luís Ramalho on 22/01/17.
 * <info@luisramalho.com>
 */

public class Link implements Serializable {
    private String url;
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
