package com.luisramalho.texttojson.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Luís Ramalho on 22/01/17.
 * <info@luisramalho.com>
 */

public class JsonStructure implements Serializable {
    private Set<String> mentions;
    private Set<String> emoticons;
    private Set<Link> links;

    public Set<String> getMentions() {
        return mentions;
    }

    public void setMentions(Set<String> mentions) {
        this.mentions = mentions;
    }

    public Set<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(Set<String> emoticons) {
        this.emoticons = emoticons;
    }

    public Set<Link> getLinks() {
        return links;
    }

    public void setLinks(Set<Link> links) {
        this.links = links;
    }

    public Set<String> getLinksUrl() {
        Set<String> urls = new HashSet<>();
        for (Link link : links) {
            urls.add(link.getUrl());
        }
        return urls;
    }
}
