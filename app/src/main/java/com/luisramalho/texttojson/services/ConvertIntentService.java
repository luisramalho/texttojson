package com.luisramalho.texttojson.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Patterns;

import com.luisramalho.texttojson.Constants;
import com.luisramalho.texttojson.models.JsonStructure;
import com.luisramalho.texttojson.models.Link;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Luís Ramalho on 22/01/17.
 * <info@luisramalho.com>
 */

public class ConvertIntentService extends IntentService {
    private static final String REGEX_MENTIONS = "@([\\w]{1,49})\\b";
    private static final String REGEX_EMOTICONS = "\\(([A-Za-z0-9]{1,15})\\)";
    private final String regex = "\\<title>(.*)\\</title>";
    private final Pattern TITLE_TAG = Pattern.compile(regex,
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    public ConvertIntentService() {
        super("ConvertIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        convert(intent.getDataString());
    }

    private void convert(String input) {
        JsonStructure json = new JsonStructure();
        if (input != null && !input.isEmpty()) {
            json.setMentions(parseMentions(input));
            json.setEmoticons(parseEmoticons(input));
            Matcher matcher = Patterns.WEB_URL.matcher(input);
            while (matcher.find()) {
                Link link = new Link();
                if (json.getLinks() == null) {
                    json.setLinks(new HashSet<Link>());
                }
                String url = matcher.group(1).toLowerCase();
                if (!json.getLinksUrl().contains(url)) {
                    link.setUrl(url);
                    json.getLinks().add(link);
                }
            }

            Set<Link> jsonLinks = json.getLinks();
            if (jsonLinks != null && !jsonLinks.isEmpty()) {
                populateLinkTitle(jsonLinks);
            }

            Intent localIntent = new Intent(Constants.BROADCAST_ACTION)
                    .putExtra(Constants.EXTENDED_DATA_STATUS, json);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
    }

    private Set<String> parseMentions(String input) {
        Set<String> mentions = new HashSet<>();
        Matcher matcher = Pattern.compile(REGEX_MENTIONS).matcher(input);
        while (matcher.find()) {
            String match = matcher.group(1);
            mentions.add(match);
        }
        return !mentions.isEmpty() ? mentions : null;
    }

    private Set<String> parseEmoticons(String input) {
        Set<String> emoticons = new HashSet<>();
        Matcher matcher = Pattern.compile(REGEX_EMOTICONS).matcher(input);
        while (matcher.find()) {
            String match = matcher.group(1).toLowerCase();
            emoticons.add(match);
        }
        return !emoticons.isEmpty() ? emoticons : null;
    }

    private void populateLinkTitle(Set<Link> links) {
        for (Link link : links) {
            try {
                String rawUrl = link.getUrl();
                if (!rawUrl.startsWith("https://") &&
                        !rawUrl.startsWith("http://")) {
                    rawUrl = "http://" + rawUrl;
                }
                URL uniformResourceLocator = new URL(rawUrl);
                String resultString = fetchTitle(uniformResourceLocator);
                if (resultString != null) {
                    link.setTitle(resultString);
                } else {
                    throw new IOException("Unknown");
                }
            } catch (Exception e) {
                link.setTitle(e.getMessage());
            }
        }
    }

    private String fetchTitle(URL url) throws IOException {
        InputStream stream = null;
        HttpURLConnection connection = null;
        String title = null;
        try {
            HttpURLConnection.setFollowRedirects(true);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(3 * 1000);
            connection.setConnectTimeout(3 * 1000);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                    String location = connection.getHeaderField("Location");
                    connection = (HttpURLConnection) new URL(location)
                            .openConnection();
                } else {
                    throw new IOException("HTTP error code: " + responseCode);
                }
            }

            String contentType = connection.getContentType();
            if (contentType == null || !contentType.contains("text/html")) {
                return null;
            } else {
                stream = connection.getInputStream();
                if (stream != null) {
                    Scanner scanner = new Scanner(stream);
                    scanner.findWithinHorizon(TITLE_TAG, 0);
                    title = scanner.match().group(1).trim();
                }
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return title;
    }
}
