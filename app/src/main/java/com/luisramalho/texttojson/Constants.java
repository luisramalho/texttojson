package com.luisramalho.texttojson;

/**
 * Created by Luís Ramalho on 18/01/17.
 * <info@luisramalho.com>
 */

public class Constants {
    public static final String BROADCAST_ACTION =
            "com.luisramalho.texttojson.BROADCAST";

    public static final String EXTENDED_DATA_STATUS =
            "com.luisramalho.texttojson.STATUS";
}
