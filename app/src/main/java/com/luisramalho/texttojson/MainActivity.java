package com.luisramalho.texttojson;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luisramalho.texttojson.models.JsonStructure;
import com.luisramalho.texttojson.services.ConvertIntentService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final String STATE_MESSAGE = "message";
    private static final String STATE_JSON_STRUCTURE = "json";
    private static final String STATE_CHECKBOX = "checkbox";

    @BindView(R.id.messageEditText) EditText messageEditText;
    @BindView(R.id.convertingProgressBar) ProgressBar progressBar;
    @BindView(R.id.jsonTextView) TextView jsonTextView;
    @BindView(R.id.convertButton) Button convertButton;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(android.R.id.content) View rootView;
    @BindView(R.id.prettyPrintCheckBox) CheckBox prettyPrintCheckBox;
    @BindView(R.id.resultFrameLayout) FrameLayout resultFrameLayout;

    private JsonStructure jsonStructure;
    private boolean prettyPrint;
    private String message;
    private int shortAnimTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        shortAnimTime = getResources().getInteger(android.R.integer
                .config_shortAnimTime);

        if (savedInstanceState != null) {
            message = savedInstanceState.getString(STATE_MESSAGE, "");
            jsonStructure = (JsonStructure) savedInstanceState
                    .getSerializable(STATE_JSON_STRUCTURE);
            prettyPrint = savedInstanceState.getBoolean(STATE_CHECKBOX,
                    prettyPrint);
        }

        if (jsonStructure != null) {
            jsonTextView.setText(formattedJson());
        }

        IntentFilter statusIntentFilter = new IntentFilter(
                Constants.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(new ResponseReceiver(), statusIntentFilter);
    }

    @OnClick(R.id.convertButton)
    public void convert(Button button) {
        String messageString = messageEditText.getText().toString();
        if (!messageString.isEmpty()) {
            if (isConnected()) {
                Intent serviceIntent = new Intent(MainActivity.this,
                        ConvertIntentService.class);
                serviceIntent.setData(Uri.parse(messageString));
                startService(serviceIntent);
                jsonTextView.setText("");
                progressBar.setAlpha(1f);
                progressBar.setVisibility(View.VISIBLE);
                button.setText(getString(R.string.converting));
                button.setEnabled(false);
            } else {
                Snackbar snackbar = Snackbar
                        .make(rootView, R.string.not_connected,
                                Snackbar.LENGTH_LONG)
                        .setAction(R.string.connect,
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(
                                                Settings.ACTION_SETTINGS));
                                    }
                                });
                snackbar.show();
            }
        } else {
            Snackbar.make(rootView, R.string.nothing_to_parse,
                        Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    @OnClick(R.id.prettyPrintCheckBox)
    public void prettyPrint(CheckBox checkBox) {
        prettyPrint = checkBox.isChecked();
        if (jsonStructure == null) return;
        TransitionManager.beginDelayedTransition(resultFrameLayout);
        jsonTextView.setText(formattedJson());
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return !(networkInfo == null ||
                !networkInfo.isConnected() ||
                (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_MESSAGE, message);
        outState.putBoolean(STATE_CHECKBOX, prettyPrint);
        outState.putSerializable(STATE_JSON_STRUCTURE, jsonStructure);
    }

    private String formattedJson() {
        if (prettyPrint) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(jsonStructure);
        } else {
            return new Gson().toJson(jsonStructure);
        }
    }

    private class ResponseReceiver extends BroadcastReceiver {
        private ResponseReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            jsonStructure = getJsonStructure(intent);

            convertButton.setText(getString(R.string.convert));
            convertButton.setEnabled(true);

            progressBar.animate()
                    .alpha(0f)
                    .setDuration(shortAnimTime)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });

            jsonTextView.setAlpha(0f);
            jsonTextView.setVisibility(View.VISIBLE);
            jsonTextView.setText(formattedJson());
            jsonTextView.animate()
                    .alpha(1f)
                    .setDuration(shortAnimTime)
                    .setListener(null);
        }

        private JsonStructure getJsonStructure(Intent intent) {
            return (JsonStructure) intent.getExtras()
                    .getSerializable(Constants.EXTENDED_DATA_STATUS);
        }
    }
}
